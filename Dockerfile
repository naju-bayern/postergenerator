FROM centos:7

RUN  yum -y update && yum -y install epel-release && yum install -y golang bash wkhtmltopdf xorg-x11-server-Xvfb && yum clean all
COPY ./ /app
ENTRYPOINT ["go", "run", "/app/server.go"]

