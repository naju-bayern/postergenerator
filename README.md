# postergenerator

This is a little web server to generate posters.

You can run it with

    docker pull registry.gitlab.com/naju-bayern/postergenerator:latest

Most of the logic happens in the embedded javascripts in pub/index.html.
server.go implements an static file server and pdf conversion.
