package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strings"
)

func main() {
	go runVirtualXServer()
	http.HandleFunc("/createPdf", createPdf)
	http.Handle("/", http.FileServer(http.Dir("/app/pub/")))
	http.ListenAndServe(":80", nil)
}

func runVirtualXServer() {
	os.Setenv("DISPLAY", ":0")
	err := exec.Command("Xvfb", ":0", "-screen", "0", "4961x3508x24+32", "-dpi", "240").Run()
	// We should never reach that code
	fmt.Fprintln(os.Stderr, "Xvfb stopped unexpectedly")
	log.Fatal(err)
}

func createPdf(rw http.ResponseWriter, r *http.Request) {
	tmpfile, err := ioutil.TempFile("", "postergenerator")
	if err != nil {
		log.Fatal(err)
	}
	if err = tmpfile.Close(); err != nil {
		log.Fatal(err)
	}
	defer os.Remove(tmpfile.Name())

	source := "localhost" + strings.TrimPrefix(r.URL.RequestURI(), "/createPdf")
	if source == "localhost" {
		source += "?print=true"
	} else {
		source += "&print=true"
	}
	err = exec.Command("wkhtmltopdf", "--no-stop-slow-scripts", "-s", "A3", "-d", "300", "--margin-top", "0", "--margin-bottom", "0", "--margin-right", "0", "--margin-left", "0", source, tmpfile.Name()).Run()
	if err != nil {
		http.Error(rw, "Creating Pdf failed", http.StatusInternalServerError)
		fmt.Fprintln(os.Stderr, "Creating Pdf failed"+source)
		return
	}
	http.ServeFile(rw, r, tmpfile.Name())
}
